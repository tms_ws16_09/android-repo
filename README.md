# christmas.io

Weihnachtsmärkte in Berlin/Brandenburg finden leicht gemacht!

## Getting Started

Im Folgenden wird erklärt wie man die benutzten Entwicklungsumgebungen für die Entwicklung von christmas.io konfiguriert.
Außerdem ist beschrieben, wie das Ausführen der entwickelten Tests für das Produkt abläuft.

### Vorbereitungen

Es muss folgende Software in der jeweils aktuellen Version (oder der unten angegebenen) installiert sein

```
Android Studio 2.2.3
```

Außerdem muss zum erfolgreichen Ausführen der App eine Internetverbindung verfügbar sein, da die verwendeten Daten aus dem Internet bezogen werden.

### Installation

Eine Schritt-Für-Schritt Anleitung welche Dir hilft die Entwicklungsumgebung aufzusetzen

Folgende Schritte müssen ausgeführt werden

```
Android Studio installieren
```

```
Projekt importieren (File -> Open -> Open File or Project)
```

```
Emulator konfigurieren (oder wahlweise Android-Gerät verbinden/konfigurieren)
```

## Ausführen der Tests

Um Tests an dem System durchzuführen werden im Emulator vom Benutzer Eingaben getätigt.

## Deployment

Die App läuft auf allen emulierten Android Geräten mit SDK 17 oder höher.
Um die App auf einem Handy zu testen befolge folgende Anweisungen unter diesem Link [Run Apps on a Hardware Device](https://developer.android.com/studio/run/device.html)

## Erstellt mit

* [AndroidStudio](https://developer.android.com/studio/index.html) - Verwendete IDE

## Versionierung

Wir benutzen [Bitbucket](http://bitbucket.org/) zur Versionierung. Für eine Übersicht aller verfügbaren Versionen, klicke [hier](https://bitbucket.org/tms_ws16_09/android-repo).

## Autoren

* **Hannes Voss** - *Entwickler* - [HannesVoss](https://bitbucket.org/link80)

## Lizenz

Das Produkt unterliegt derzeit keiner Lizenz.

## Erwähnungen

* Danke an alle Autoren von genutzten Fremd-Libraries