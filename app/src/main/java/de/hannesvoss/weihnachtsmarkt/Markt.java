package de.hannesvoss.weihnachtsmarkt;

import java.util.LinkedList;

/**
 * Created by Hannes on 06.01.2017.
 */

public class Markt {
    static LinkedList<Markt> list = new LinkedList<Markt>();

    private int id;
    private String title;
    private String adress;
    private String region;
    private String city;
    private String date;
    private String times;
    private String mail;
    private String website;
    private String notes;

    Markt() {
        list.add(this);
    }

    public static Markt findById(int id) {
        for (int i=0; i < list.size(); i++) {
            if (list.get(i).getId() == id)
                return list.get(i);
        }
        return null;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getAdress() {
        return this.adress;
    }

    public String getRegion() {
        return this.region;
    }

    public String getCity() {
        return this.city;
    }

    public String getDate() {
        return this.date;
    }

    public String getTimes() {
        return this.times;
    }

    public String getMail() {
        return this.mail;
    }

    public String getWebsite() {
        return this.website;
    }

    public String getNotes() {
        return this.notes;
    }
}
