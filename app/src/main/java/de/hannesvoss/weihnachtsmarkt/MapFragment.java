package de.hannesvoss.weihnachtsmarkt;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

public class MapFragment extends Fragment {
    private GoogleMap map;
    public static JSONObject rootObject;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            getJSONStuff();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_map, container, false);

        MapView myOpenMapView = (MapView) view.findViewById(R.id.map);
        myOpenMapView.onCreate(savedInstanceState);
        myOpenMapView.onResume();

        myOpenMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {
                loadMap(map);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(52.5170365, 13.3888599), 9.5f));

                // Eigene Berechtigungen werden geprüft (intern) -> Alles ok zur Anzeige des eigenen Standortes?
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    map.setMyLocationEnabled(true);
                } else {
                    Toast.makeText(getContext(), getString(R.string.myLocationError), Toast.LENGTH_SHORT).show();
                }

                setMarkers();
            }
        });

        return view;
    }

    protected void loadMap(GoogleMap googleMap) {
        map = googleMap;
    }

    private void setMarkers() {
        if (rootObject != null) {
            try {
                JSONArray featuresArray = rootObject.getJSONArray(getString(R.string.features));

                for (int i = 0; i < featuresArray.length(); i++) {
                    try {
                        JSONObject containerObject = featuresArray.getJSONObject(i);
                        JSONObject propertiesObject = containerObject.getJSONObject(getString(R.string.properties));
                        JSONObject dataObject = propertiesObject.getJSONObject(getString(R.string.data));

                        // Pulling items from the array
                        String id       = dataObject.getString(getString(R.string.id));
                        String name     = dataObject.getString(getString(R.string.name));
                        String latitude = dataObject.getString(getString(R.string.lat));
                        String longitude = dataObject.getString(getString(R.string.lng));

                        // Convert in the right double format
                        latitude = latitude.replace(",", ".");
                        longitude = longitude.replace(",", ".");

                        // Change the size of the marker image
                        BitmapDrawable markerImg = (BitmapDrawable)getResources().getDrawable(R.mipmap.maps_marker_red);
                        Bitmap tmp = markerImg.getBitmap();
                        Bitmap scaledMarker = Bitmap.createScaledBitmap(tmp, 35, 35, false);

                        // Setting Markers on Map!
                        Marker marker = map.addMarker(
                                new MarkerOptions()
                                        .position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)))
                                        .title(name)
                                        .icon(BitmapDescriptorFactory.fromBitmap(scaledMarker)));
                        marker.setTag(Integer.parseInt(id));

                        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                            @Override
                            public View getInfoWindow(Marker marker) {
                                return null;
                            }

                            @Override
                            public View getInfoContents(Marker marker) {
                                map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                                    @Override
                                    public void onInfoWindowClick(Marker marker) {
                                        //  Setzen der aktuellen Markt-Referenz für die Detail-Anzeige
                                        MainActivity.currentMarket = Markt.findById((Integer) marker.getTag());

                                        //  Ändern des angezeigten Fragments
                                        TabHost mTabHost = (TabHost)getActivity().findViewById(android.R.id.tabhost);
                                        mTabHost.setCurrentTab(2);
                                    }
                                });

                                return null;
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void getJSONStuff() {
        //  Getting the JSON stuff..
        try {
            JSONGetter abc = new JSONGetter(getContext());
            //  Use GJSON for getting the geo data to visualize the positions in maps fragment
            abc.execute(new URL(getString(R.string.dataURL)));
            String result = abc.get();
            MapFragment.rootObject = new JSONObject(result);
            JSONArray featuresArray = MapFragment.rootObject.getJSONArray(getString(R.string.features));

            MainActivity.data = new HashMap[featuresArray.length()];
            for (int i = 0; i < featuresArray.length(); i++) {
                try {
                    JSONObject containerObject = featuresArray.getJSONObject(i);
                    JSONObject propertiesObject = containerObject.getJSONObject(getString(R.string.properties));
                    JSONObject dataObject = propertiesObject.getJSONObject(getString(R.string.data));

                    // Storing JSON data in an array of HashMap's
                    MainActivity.data[i] = jsonToMap(dataObject.toString());

                    Markt tmp = new Markt();
                    tmp.setId(Integer.parseInt(dataObject.getString(getString(R.string.id))));
                    tmp.setTitle(dataObject.getString(getString(R.string.name)));
                    tmp.setAdress(dataObject.getString(getString(R.string.strasse)));
                    tmp.setRegion(dataObject.getString(getString(R.string.bezirk)));
                    tmp.setCity(dataObject.getString(getString(R.string.plz_ort)));
                    tmp.setDate(dataObject.getString("von") + dataObject.getString("bis"));
                    tmp.setTimes(dataObject.getString(getString(R.string.oeffnungszeiten)));
                    tmp.setMail(dataObject.getString(getString(R.string.email)));
                    tmp.setWebsite(dataObject.getString(getString(R.string.w3)));
                    tmp.setNotes(dataObject.getString(getString(R.string.bemerkungen)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static HashMap<String, String> jsonToMap(String t) throws JSONException {
        HashMap<String, String> map = new HashMap<String, String>();
        JSONObject jObject = new JSONObject(t);
        Iterator<?> keys = jObject.keys();

        while(keys.hasNext()) {
            String key = (String)keys.next();
            String value = jObject.getString(key);
            map.put(key, value);
        }

        return map;
    }
}
