package de.hannesvoss.weihnachtsmarkt;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;

public class MainActivity extends FragmentActivity {
    private static final int REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final int REQUEST_INTERNET = 2;

    static HashMap<String, String>[] data;
    public static Markt currentMarket = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkPermissions();

        setContentView(R.layout.activity_main);

        FragmentTabHost mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);

        mTabHost.addTab(mTabHost.newTabSpec(getString(R.string.menu_item_1)).setIndicator(getTabIndicator(mTabHost.getContext(), R.string.menu_item_1, R.drawable.ic_tab_karte)), MapFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec(getString(R.string.menu_item_2)).setIndicator(getTabIndicator(mTabHost.getContext(), R.string.menu_item_2, R.drawable.ic_tab_liste)), MaerkteFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec(getString(R.string.menu_item_3)).setIndicator(getString(R.string.menu_item_3)), DetailFragment.class, null);
        mTabHost.getTabWidget().getChildTabViewAt(2).setVisibility(View.GONE);
    }

    private View getTabIndicator(Context context, int title, int icon) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);

        ImageView iv = (ImageView) view.findViewById(R.id.imageView);
        iv.setImageResource(icon);

        TextView tv = (TextView) view.findViewById(R.id.textView);
        tv.setText(title);

        return view;
    }

    private void checkPermissions(){
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_FINE_LOCATION);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, REQUEST_INTERNET);
            }
        }
    }
}
