package de.hannesvoss.weihnachtsmarkt;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Map;

public class DetailFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        //  Put stuff here..
        TextView txtTitle = (TextView) view.findViewById(R.id.txtTitel);
        TextView txtAdress = (TextView) view.findViewById(R.id.txtAdress);
        TextView txtCity = (TextView) view.findViewById(R.id.txtCity);
        TextView txtDate = (TextView) view.findViewById(R.id.txtDate);
        TextView txtTimes = (TextView) view.findViewById(R.id.txtTimes);
        TextView txtMail = (TextView) view.findViewById(R.id.txtMail);
        TextView txtWebsite = (TextView) view.findViewById(R.id.txtWebsite);
        TextView txtNotes = (TextView) view.findViewById(R.id.txtNotes);

        if (MainActivity.currentMarket != null) {
            txtTitle.setText(MainActivity.currentMarket.getTitle());
            txtAdress.setText(MainActivity.currentMarket.getAdress());
            txtCity.setText(MainActivity.currentMarket.getCity());
            txtDate.setText(MainActivity.currentMarket.getDate());
            txtTimes.setText(MainActivity.currentMarket.getTimes());
            txtMail.setText(MainActivity.currentMarket.getMail());
            txtWebsite.setText(MainActivity.currentMarket.getWebsite());
            txtNotes.setText(MainActivity.currentMarket.getNotes());
        }

        return view;
    }
}
