package de.hannesvoss.weihnachtsmarkt;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;
import android.widget.TabHost;

import java.util.ArrayList;
import java.util.ListIterator;

public class MaerkteFragment extends ListFragment {
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {
            ArrayList<Markt> list = new ArrayList<Markt>();
            MarktAdapter adapter = new MarktAdapter(getActivity(), list);
            ListView lv = (ListView) getView().findViewById(android.R.id.list);

            ListIterator<Markt> iterator = Markt.list.listIterator();
            while(iterator.hasNext()) {
                adapter.add(iterator.next());
            }

            setListAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        //  Setzen der aktuellen Markt-Referenz für die Detail-Anzeige
        MainActivity.currentMarket = (Markt) l.getItemAtPosition((int)id);

        //  Ändern des angezeigten Fragments
        TabHost mTabHost = (TabHost)getActivity().findViewById(android.R.id.tabhost);
        mTabHost.setCurrentTab(2);
    }
}
