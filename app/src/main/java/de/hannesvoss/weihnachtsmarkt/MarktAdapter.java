package de.hannesvoss.weihnachtsmarkt;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Hannes on 20.01.2017.
 */

public class MarktAdapter extends ArrayAdapter<Markt> {
    Context context;
    ArrayList<Markt> maerkte = null;

    public MarktAdapter(Context context, ArrayList<Markt> maerkte) {
        super(context, 0, maerkte);
        this.context = context;
        this.maerkte = maerkte;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Markt markt = getItem(position);

        if (convertView == null)
            convertView = LayoutInflater.from((Activity) context).inflate(android.R.layout.simple_list_item_1, parent, false);

        TextView tmp = (TextView) convertView.findViewById(android.R.id.text1);
        tmp.setText(markt.getTitle());

        return convertView;
    }
}
